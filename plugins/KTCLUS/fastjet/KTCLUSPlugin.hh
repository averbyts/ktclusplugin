//FJSTARTHEADER
// $Id$
//
// Copyright (c) 2007-2021, Matteo Cacciari, Gavin P. Salam and Gregory Soyez
//
//----------------------------------------------------------------------
// This file is part of FastJet.
//
//  FastJet is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  The algorithms that underlie FastJet have required considerable
//  development. They are described in the original FastJet paper,
//  hep-ph/0512210 and in the manual, arXiv:1111.6097. If you use
//  FastJet as part of work towards a scientific publication, please
//  quote the version you use and include a citation to the manual and
//  optionally also to hep-ph/0512210.
//
//  FastJet is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with FastJet. If not, see <http://www.gnu.org/licenses/>.
//----------------------------------------------------------------------
//FJENDHEADER

#ifndef __KTCLUSPLUGIN_HH__
#define __KTCLUSPLUGIN_HH__
#include <string>
#include "fastjet/JetDefinition.hh"

FASTJET_BEGIN_NAMESPACE      // defined in fastjet/internal/base.hh
// forward declaration to reduce includes
class ClusterSequence;

class KTCLUSPlugin : public JetDefinition::Plugin {
public:
    enum Strategy { strategy_NNH = 0, strategy_NNFJN2Plain = 1};
    KTCLUSPlugin (Strategy strategy = strategy_NNFJN2Plain) : _strategy(strategy) {}

    KTCLUSPlugin (const int x,const double radius)
        : _ktclustype(x) {
        if (radius!=1) std::cout<< "Forced radius to 1.0"<<std::endl;
        _R = 1.0;
    }

    KTCLUSPlugin (const KTCLUSPlugin & plugin) {
        *this = plugin;
    }

    virtual std::string description () const;
    virtual void run_clustering(ClusterSequence &) const;

    virtual double R() const {
        return _R;
    }

    virtual bool exclusive_sequence_meaningful() const {
        return true;
    }

    //void test(std::vector<PseudoJet>&);
private:

    double _R;

    template<class N> void _actual_run_clustering(ClusterSequence &) const;

    Strategy _strategy;
    int _ktclustype;
};

FASTJET_END_NAMESPACE        // defined in fastjet/internal/base.hh

#endif // __KTCLUSPLUGIN_HH__

