//FJSTARTHEADER
// $Id$
//
// Copyright (c) 2007-2021, Matteo Cacciari, Gavin P. Salam and Gregory Soyez
// Modifications for KTCLUSPlugin by Andrii verbytskyi , 2021
//----------------------------------------------------------------------
// This file is part of FastJet.
//
//  FastJet is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  The algorithms that underlie FastJet have required considerable
//  development. They are described in the original FastJet paper,
//  hep-ph/0512210 and in the manual, arXiv:1111.6097. If you use
//  FastJet as part of work towards a scientific publication, please
//  quote the version you use and include a citation to the manual and
//  optionally also to hep-ph/0512210.
//
//  FastJet is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with FastJet. If not, see <http://www.gnu.org/licenses/>.
//----------------------------------------------------------------------
//FJENDHEADER

#include <vector>
#include <sstream>
#include <limits>
#include <iostream>

// fastjet stuff
#include "fastjet/ClusterSequence.hh"
#include "fastjet/KTCLUSPlugin.hh"
#include "fastjet/NNH.hh"

using namespace std;


FASTJET_BEGIN_NAMESPACE      // defined in fastjet/internal/base.hh

inline double to_pi(const double& a) {
    return ( a > M_PI && a < 2 * M_PI ) ? 2 * M_PI - a : a;
}
class KTCLUSInfo {
public:
    KTCLUSInfo(const double R): _R(R) {}
    double R() const {
        return _R;
    }
private:
    double _R;
};

template<int TYPE, int ANGLE, int MONO, int RECOM>
class KTCLUSBriefJet {
public:
    void init(const PseudoJet & jet, KTCLUSInfo* IN) {
        double norm = 1.0/sqrt(jet.modp2());
        nx = jet.px() * norm;
        ny = jet.py() * norm;
        nz = jet.pz() * norm;
        E = jet.E();
        pt = jet.pt();
        phi = jet.phi();
        eta = jet.eta();
        rap = jet.rap();
        pseudorapidity = jet.pseudorapidity();
        R = IN->R();
        IR2 = 1.0/(R*R);
    }

    double distance(const KTCLUSBriefJet * jet) const {
        if (TYPE == 1 && ANGLE == 1) return 2*R*R*min(E*E,jet->E*jet->E)*(1.0-(nx*jet->nx + ny*jet->ny + nz*jet->nz));
        if (TYPE == 2 && ANGLE == 1) return 2*R*R*min(E*E,jet->E*jet->E)*(1.0-(nx*jet->nx + ny*jet->ny + nz*jet->nz));
        if (TYPE == 3 && ANGLE == 1) return 2*R*R*min(E*E,jet->E*jet->E)*(1.0-(nx*jet->nx + ny*jet->ny + nz*jet->nz));
        if (TYPE == 4 && ANGLE == 1) return 2*R*R*min(E*E,jet->E*jet->E)*(1.0-(nx*jet->nx + ny*jet->ny + nz*jet->nz));

        if (TYPE == 1 && ANGLE == 2) return R*R*min(pt*pt,jet->pt*jet->pt)* ( pow(to_pi(phi-jet->phi),2)  + pow(rap-jet->rap,2)); ///DOES NOT WORK!!!
        if (TYPE == 2 && ANGLE == 2) return R*R*min(pt*pt,jet->pt*jet->pt)* ( pow(to_pi(phi-jet->phi),2)  + pow(rap-jet->rap,2));
        if (TYPE == 3 && ANGLE == 2) return R*R*min(pt*pt,jet->pt*jet->pt)* ( pow(to_pi(phi-jet->phi),2)  + pow(rap-jet->rap,2));
        if (TYPE == 4 && ANGLE == 2) return R*R*min(pt*pt,jet->pt*jet->pt)* ( pow(to_pi(phi-jet->phi),2)  + pow(rap-jet->rap,2));

        if (TYPE == 1 && ANGLE == 3) return R*R*min(pt*pt,jet->pt*jet->pt)* 2*(cosh(rap-jet->rap)-cos(to_pi(phi-jet->phi)));
        if (TYPE == 2 && ANGLE == 3) return R*R*min(pt*pt,jet->pt*jet->pt)* 2*(cosh(rap-jet->rap)-cos(to_pi(phi-jet->phi)));
        if (TYPE == 3 && ANGLE == 3) return R*R*min(pt*pt,jet->pt*jet->pt)* 2*(cosh(rap-jet->rap)-cos(to_pi(phi-jet->phi)));
        if (TYPE == 4 && ANGLE == 3) return R*R*min(pt*pt,jet->pt*jet->pt)* 2*(cosh(rap-jet->rap)-cos(to_pi(phi-jet->phi)));

        cerr<<"Unknown distance function"<<endl;
    }

    double beam_distance() const {
        if (TYPE == 1 && ANGLE == 1) return numeric_limits<double>::max()/100;
        if (TYPE == 2 && ANGLE == 1) return 2*R*R*E*E*(1+nz);
        if (TYPE == 3 && ANGLE == 1) return 2*R*R*E*E*(1-nz);
        if (TYPE == 4 && ANGLE == 1) return 2*R*R*E*E*std::min(1-nz,1+nz);

        if (TYPE == 1 && ANGLE == 2) return numeric_limits<double>::max()/100; ///DOES NOT WORK!!!
        if (TYPE == 2 && ANGLE == 2) return R*R*pt*pt;
        if (TYPE == 3 && ANGLE == 2) return R*R*pt*pt;
        if (TYPE == 4 && ANGLE == 2) return R*R*pt*pt;

        if (TYPE == 1 && ANGLE == 3) return numeric_limits<double>::max()/100;
        if (TYPE == 2 && ANGLE == 3) return R*R*pt*pt;
        if (TYPE == 3 && ANGLE == 3) return R*R*pt*pt;
        if (TYPE == 4 && ANGLE == 3) return R*R*pt*pt;
        cerr<<"Unknown beam distance function"<<endl;
    }

private:
    double E, pt, eta, rap, phi, pseudorapidity, nx, ny, nz, R, IR2;
};

template<int TYPE, int ANGLE>
double beam_distance(const PseudoJet& jet) {
    double norm = 1.0/sqrt(jet.modp2());
    double nz = jet.pz() * norm;
    double E = jet.E();
    double pt = jet.pt();
    if (TYPE == 1 && ANGLE == 1) return numeric_limits<double>::max()/100;
    if (TYPE == 2 && ANGLE == 1) return 2*E*E*(1+nz);
    if (TYPE == 3 && ANGLE == 1) return 2*E*E*(1-nz);
    if (TYPE == 4 && ANGLE == 1) return 2*E*E*std::min(1-nz,1+nz);

    if (TYPE == 1 && ANGLE == 2) return numeric_limits<double>::max()/100; ///DOES NOT WORK!!!
    if (TYPE == 2 && ANGLE == 2) return pt*pt;
    if (TYPE == 3 && ANGLE == 2) return pt*pt;
    if (TYPE == 4 && ANGLE == 2) return pt*pt;

    if (TYPE == 1 && ANGLE == 3) return numeric_limits<double>::max()/100;
    if (TYPE == 2 && ANGLE == 3) return pt*pt;
    if (TYPE == 3 && ANGLE == 3) return pt*pt;
    if (TYPE == 4 && ANGLE == 3) return pt*pt;
    cerr<<"Unknown  beam distance "<<TYPE <<" "<< ANGLE<<endl;
}

/*
void KTCLUSPlugin::test(vector<PseudoJet>& X)
{


KTCLUSBriefJet<4,2,1,1> Aj;
KTCLUSBriefJet<4,2,1,1> Bj;
Aj.init(X[0]);
Bj.init(X[1]);
printf("%10.6f\n",Aj.distance(&Bj));

}
* */
//----------------------------------------------------------------------

template<class N> void KTCLUSPlugin::_actual_run_clustering(ClusterSequence & cs) const {

    int njets = cs.jets().size();
    KTCLUSInfo  IN(_R);
    N nn(cs.jets(),&IN);

    while (njets > 0) {
        int i, j, k;
        double dij = nn.dij_min(i, j);

        if (j >= 0) {
            cs.plugin_record_ij_recombination(i, j, dij, k);
            nn.merge_jets(i, j, cs.jets()[k], k);
        } else {
            double diB=cs.jets()[i].beam_distance();
            switch (_ktclustype/10) {
                break;
            case  111:
                diB=beam_distance<1,1>(cs.jets()[i]);
                break;
            case  211:
                diB=beam_distance<2,1>(cs.jets()[i]);
                break;
            case  311:
                diB=beam_distance<3,1>(cs.jets()[i]);
                break;
            case  411:
                diB=beam_distance<4,1>(cs.jets()[i]);
                break;
            case  121:
                diB=beam_distance<1,2>(cs.jets()[i]);
                break;
            case  221:
                diB=beam_distance<2,2>(cs.jets()[i]);
                break;
            case  321:
                diB=beam_distance<3,2>(cs.jets()[i]);
                break;
            case  421:
                diB=beam_distance<4,2>(cs.jets()[i]);
                break;
            case  131:
                diB=beam_distance<1,3>(cs.jets()[i]);
                break;
            case  231:
                diB=beam_distance<2,3>(cs.jets()[i]);
                break;
            case  331:
                diB=beam_distance<3,3>(cs.jets()[i]);
                break;
            case  431:
                diB=beam_distance<4,3>(cs.jets()[i]);
                break;
            default:
                cerr << "Unknown mode in _actual_run_clustering "<< _ktclustype <<endl;
            }
            diB*=(_R*_R);
            cs.plugin_record_iB_recombination(i, diB);
            nn.remove_jet(i);
        }
        njets--;
    }

}

string KTCLUSPlugin::description () const {
    ostringstream desc;
    desc << " KTCLUS algorithm plugin in mode "<<_ktclustype;
    switch(_strategy) {
    case strategy_NNH:
        desc << ", using NNH strategy";
        break;
    case strategy_NNFJN2Plain:
        desc << ", using NNFJN2Plain strategy";
        break;
    default:
        throw Error("Unrecognized strategy in KTCLUSPlugin");
    }

    return desc.str();
}

//----------------------------------------------------------------------

void KTCLUSPlugin::run_clustering(ClusterSequence & cs) const {


    switch (_ktclustype/10) {
    case 111:
        _actual_run_clustering<NNH<KTCLUSBriefJet < 1,1,1,0 >,KTCLUSInfo> >(cs);
        break;
    case 211:
        _actual_run_clustering<NNH<KTCLUSBriefJet < 2,1,1,0 >,KTCLUSInfo> >(cs);
        break;
    case 311:
        _actual_run_clustering<NNH<KTCLUSBriefJet < 3,1,1,0 >,KTCLUSInfo > >(cs);
        break;
    case 411:
        _actual_run_clustering<NNH<KTCLUSBriefJet < 4,1,1,0 >,KTCLUSInfo> >(cs);
        break;
    case 121:
        _actual_run_clustering<NNH<KTCLUSBriefJet < 1,2,1,0 >,KTCLUSInfo > >(cs);
        break;
    case 221:
        _actual_run_clustering<NNH<KTCLUSBriefJet < 2,2,1,0 >,KTCLUSInfo > >(cs);
        break;
    case 321:
        _actual_run_clustering<NNH<KTCLUSBriefJet < 3,2,1,0 >,KTCLUSInfo> >(cs);
        break;
    case 421:
        _actual_run_clustering<NNH<KTCLUSBriefJet < 4,2,1,0 >,KTCLUSInfo> >(cs);
        break;
    case 131:
        _actual_run_clustering<NNH<KTCLUSBriefJet < 1,3,1,0 >,KTCLUSInfo > >(cs);
        break;
    case 231:
        _actual_run_clustering<NNH<KTCLUSBriefJet < 2,3,1,0 >,KTCLUSInfo > >(cs);
        break;
    case 331:
        _actual_run_clustering<NNH<KTCLUSBriefJet < 3,3,1,0 >,KTCLUSInfo > >(cs);
        break;
    case 431:
        _actual_run_clustering<NNH<KTCLUSBriefJet < 4,3,1,0 >,KTCLUSInfo > >(cs);
        break;
    default:
        cerr<<"Unknown mode in run_clustering " << _ktclustype << endl;
    }
}

FASTJET_END_NAMESPACE      // defined in fastjet/internal/base.hh
