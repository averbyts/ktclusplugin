#include <vector>
#include <iostream>

#ifndef USE_FJCORE
#include <fastjet/ClusterSequence.hh>
#include "fastjet/KTCLUSPlugin.hh"
using namespace fastjet;
#else
#include <fjcore.hh>
using namespace fjcore;
#endif

using namespace std;
///

#include "ktclus.h"
int main()
{
    srand(1);
    vector<PseudoJet> input_particles;
    double PP[100][4];
    int N = 20;
    freal ECUT = 2.0;
    for (size_t i = 0; i < N; i++) {
        double x = 10*(drand48()-0.5);
        double y = 10*(drand48()-0.5);
        double z = 10*(drand48()-0.3);
        double e = sqrt(x*x + y*y + z*z + 0.139*0.139);
        auto pj = PseudoJet(x, y, z, e);
        PP[i][0] = x;
        PP[i][1] = y;
        PP[i][2] = z;
        PP[i][3] = e;
        input_particles.push_back(pj);
        cout<<pj.px()<<" "<<pj.py()<<" "<<pj.pz()<<" "<<pj.e()<<endl;
    }
    std::vector<int> modes = {4111,3111,2111};
    for (auto mode: modes) {
        freal R1=1.0;
        freal Y[100];
        int* label;
        ktclur_(mode, &(PP[0][0]), N, R1, ECUT, Y, label);
        cout<<"KTCLUS Y, MODE="<<mode<<endl;
        for (int i=0; i<10; i++)  cout<<Y[i]<<" ";
        cout<<endl;
        freal YCUT =15;
        freal YMAC =0.001;
        double PJET[100][4];
        int JET[100];
        int NSUB;
        int NJET;
        int *c;
        int recom = mode%10;
        ktreco_(recom, &(PP[0][0]), N, ECUT,  YCUT,  YMAC, &(PJET[0][0]), JET, NJET, NSUB, c);
        cout<<"KTCLUS JETS, MODE="<<mode<<endl;
        vector<PseudoJet> kpj;
        for (int j=0; j<NJET; j++) kpj.push_back(PseudoJet(PJET[j][0],PJET[j][1],PJET[j][2],PJET[j][3]));
        sort(kpj.begin(),kpj.end(),  [](const PseudoJet & a, const PseudoJet & b) -> bool { return a.Et() > b.Et(); });
        //for (auto j: kpj) cout<<j.px()<<" "<<j.py()<<" "<<j.pz()<<" "<<j.e()<<endl;
        for (auto j: kpj) cout<<j.px()<<" "<<j.py()<<" "<<j.py()<<" "<<j.E()<<endl;
    }

    std::vector<ClusterSequence> css;
    //  css.push_back(ClusterSequence(input_particles, JetDefinition(kt_algorithm,  1.0, RecombinationScheme::E_scheme )));
    css.push_back(ClusterSequence(input_particles, JetDefinition(new KTCLUSPlugin(4111,1.0))));
    css.push_back(ClusterSequence(input_particles, JetDefinition(new KTCLUSPlugin(3111,1.0))));
    css.push_back(ClusterSequence(input_particles, JetDefinition(new KTCLUSPlugin(2111,1.0))));
    for (auto cs: css) {
        cout<<"FASTJET d/4"<<endl;
        for (int i=0; i<10; i++)
            cout<<cs.exclusive_dmerge(i)/4<<" ";
        cout<<endl;
        cout<<"FASTJET JETS"<<endl;
        auto ijets = cs.exclusive_jets(4.0*15);
        sort(ijets.begin(),ijets.end(),  [](const PseudoJet & a, const PseudoJet & b) -> bool { return a.Et() > b.Et(); });
        for (auto j: ijets) cout<<j.px()<<" "<<j.py()<<" "<<j.py()<<" "<<j.E()<<endl;
    }
    return 0;
}
