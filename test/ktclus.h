#ifndef FLOATKTCLUS
#define freal double
extern "C" {
    void ktclur_(int& IMODE, double* PP, int& NN, double& R, double& ECUT, double* Y, int* label);
    void ktreco_(int &RECO,  double* PP, int& NN,          double & ECUT,double& YCUT, double& YMAC,double* PJET,int* JET,int& NJET,int& NSUB,int *);
    double ktpair_(int& ANGL,double* P,double* Q,double& ANGLE);
}

#else
#define freal float
extern "C" {
    void ktclur_(int& IMODE, double* PP, int& NN, float& R, float& ECUT, float* Y, int* label);
    void ktreco_(int &RECO,  double* PP, int& NN,          float & ECUT,float& YCUT, float& YMAC,double* PJET,int* JET,int& NJET,int& NSUB,int *);
    double ktpair_(int& ANGL,double* P,double* Q,float& ANGLE);
}
#endif
//COMPONENTS OF MOMENTA ARE PX,PY,PZ,E,1/P,PT,ETA,PHI,PT**2
