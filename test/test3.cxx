#include <vector>
#include <iostream>

#ifndef USE_FJCORE
#include <fastjet/ClusterSequence.hh>
#include "fastjet/KTCLUSPlugin.hh"
using namespace fastjet;
#else
#include <fjcore.hh>
using namespace fjcore;
#endif


using namespace std;
#include "ktclus.h"
int main( int argc, char** argv)
{
    int mode = 4211;
    double R1=1.0;
    auto X= new KTCLUSPlugin(mode, R1);

    double Q1[9] = {

        -2.5845402418254437,    -7.2485083211940449,  17.517183947038284,
            19.522818617816359,       5.2265664262494724e-2,  2.7068443100335795,
            1.4580189621627526,      -1.9133047315679004,     59.220721144034428


        };
    double P1[9] = {
        2.8448364392793479,   -3.9908372660751112,   -0.46689374385695703,
        4.9251585983192703,    0.20312005617594081,    4.9010076974582963,
        -9.5083218345279427e-2, -0.95150346324865620,    24.019876450545468
    };


    PseudoJet B(Q1[0],Q1[1],Q1[2],Q1[3]);
    PseudoJet A(  P1[0],P1[1],P1[2],P1[3]);

    vector<PseudoJet> v;
    v.push_back(A);
    v.push_back(B);

    //X->test(v);
    //COMPONENTS OF MOMENTA ARE PX,PY,PZ,E,1/P,PT,ETA,PHI,PT**2
    double P[9] = {v[0].px(),v[0].py(),v[0].pz(),v[0].e(),1/v[0].modp(),v[0].pt(),v[0].rap(),v[0].phi_std(),v[0].perp2()};
    double Q[9] = {v[1].px(),v[1].py(),v[1].pz(),v[1].e(),1/v[1].modp(),v[1].pt(),v[1].rap(),v[1].phi_std(),v[1].perp2()};
    for (int i=0; i<9; i++) printf("%f %f\n",P[i], P1[i]);
    int two=2;
    std::cout<<ktpair_(two,P,Q,R1)<<endl;
    return 1;
}
