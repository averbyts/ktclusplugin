#include <vector>
#include <iostream>

#ifndef USE_FJCORE
#include <fastjet/ClusterSequence.hh>
#include "fastjet/KTCLUSPlugin.hh"
using namespace fastjet;
#else
#include <fjcore.hh>
using namespace fjcore;
#endif

#define PIMASS 0.139
//#define PIMASS 0.0

using namespace std;
#include "ktclus.h"
int main( int argc, char** argv)
{
    if (argc<4) return 1;
    srand(1);
    vector<PseudoJet> input_particles;
    double PP[100][4];
    int N = 23;
    for (size_t i = 0; i < N; i++) {
        double x = 10*(drand48()-0.5);
        double y = 10*(drand48()-0.5);
        double z = 10*(drand48()-0.3);
        double e = sqrt(x*x + y*y + z*z + PIMASS*PIMASS);
        auto pj = PseudoJet(x, y, z, e);
        PP[i][0] = x;
        PP[i][1] = y;
        PP[i][2] = z;
        PP[i][3] = e;
        input_particles.push_back(pj);
    }

    int mode = atoi(argv[1]);
    freal R1 = atof(argv[2]);
    freal YCUT = atof(argv[3]);
    freal ECUT = 2.0;

    freal Y[100];
    int* label;
    ktclur_(mode, &(PP[0][0]), N, R1, ECUT, Y, label);
    freal YMAC = 0.001;
    double PJET[100][4];
    int JET[100];
    int NSUB;
    int NJET;
    int *c;
    int recom = mode%10;
    ktreco_(recom, &(PP[0][0]), N, ECUT,  YCUT,  YMAC, &(PJET[0][0]), JET, NJET, NSUB, c);
    vector<PseudoJet> kpj;
    vector<double> ky;
    for (int j = 0; j < NJET; j++) kpj.push_back(PseudoJet(PJET[j][0], PJET[j][1], PJET[j][2], PJET[j][3]));
    sort(kpj.begin(),kpj.end(), [](const PseudoJet & a, const PseudoJet & b) -> bool { return a.Et() > b.Et(); });
    for (int i = 0; i < NJET; i++)  ky.push_back(Y[i]);
    //for (auto j: kpj) cout<<j.px()<<" "<<j.py()<<" "<<j.py()<<" "<<j.E()<<endl;
    RecombinationScheme rs;
    if (mode%10 == 1) rs = E_scheme;
    if (mode%10 == 2) rs = pt_scheme;
    if (mode%10 == 3) rs = pt2_scheme;


    JetDefinition JD(new KTCLUSPlugin(mode, R1));
    JD.set_recombination_scheme(rs);
    ClusterSequence cs(input_particles, JD);
    auto fpj = cs.exclusive_jets(4.0*YCUT);
    vector<double> fy;
    sort(fpj.begin(),fpj.end(),  [](const PseudoJet & a, const PseudoJet & b) -> bool { return a.Et() > b.Et(); });
    for (int i = 0; i < fpj.size(); i++) fy.push_back(cs.exclusive_dmerge(i)/4);
    //for (auto j: fpj) cout<<j.px()<<" "<<j.py()<<" "<<j.py()<<" "<<j.E()<<endl;
    bool ok = true;
    ok = ok&&(fpj.size() == kpj.size());
    ok = ok&&(ky.size() == fy.size());
    for (int i = 0; i< std::max(fpj.size(), kpj.size()); i++)
    {
        if (i >= fpj.size()) continue;
        if (i >= kpj.size()) continue;
        if (std::abs(fpj[i].px() - kpj[i].px()) > 10e-3) ok = false;
        if (std::abs(fpj[i].py() - kpj[i].py()) > 10e-3) ok = false;
        if (std::abs(fpj[i].pz() - kpj[i].pz()) > 10e-3) ok = false;
        if (std::abs(fpj[i].e() - kpj[i].e()) > 10e-3) ok = false;
    }
    for (int i = 0; i< std::max(fy.size(), ky.size()); i++)
    {
        if (i >= fy.size()) continue;
        if (i >= ky.size()) continue;
        if (std::abs(fy[i] - ky[i]) > 10e-3 && std::abs(fy[i]) < 10e10 ) ok = false;
    }
    if (!ok) {
        cout<<mode<<endl;
        for (auto j: ky) cout<<j<<" ";
        cout<<endl;
        for (auto j: kpj) cout<<j.px()<<" "<<j.py()<<" "<<j.py()<<" "<<j.E()<<endl;
        cout<<"============="<<endl;
        for (auto j: fy) cout<<j<<" ";
        cout<<endl;
        for (auto j: fpj) cout<<j.px()<<" "<<j.py()<<" "<<j.py()<<" "<<j.E()<<endl;
    }
    if (ok) return 0;
    return 1;
}
